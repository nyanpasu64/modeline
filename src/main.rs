#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use eframe::egui;
use regex::Regex;

fn main() {
    let options = eframe::NativeOptions {
        ..Default::default()
    };
    eframe::run_native(
        "X11 modeline to timings calculator",
        options,
        Box::new(|_cc| Box::new(MyApp::new())),
    );
}

struct MyApp {
    modeline_text: String,
    modeline: Option<Modeline>,
    re_modeline: Regex,
    re_name: Regex,
    re_word: Regex,
}

impl MyApp {
    fn new() -> MyApp {
        let mut app = MyApp {
            modeline_text: r#"Modeline "NTSC 640x480 (60Hz)" 12.336 640 662 720 784 480 488 494 525 interlace -hsync -vsync"#.to_owned(),
            modeline: None,
            re_modeline: Regex::new(r"^Modeline\s+(.*)").unwrap(),
            re_name: Regex::new(r#""([^"]*)"\s+(.*)"#).unwrap(),
            re_word: Regex::new(r"([^\s]+)(?:$|\s+)(.*)").unwrap(),
        };
        app.modeline = app.parse_modeline();
        app
    }
}

struct Modeline {
    name: String,
    mpx_per_s: f64,

    hdisplay: i32,
    hsync_start: i32,
    hsync_end: i32,
    htotal: i32,

    vdisplay: i32,
    vsync_start: i32,
    vsync_end: i32,
    vtotal: i32,

    interlace: bool,
}

impl MyApp {
    fn parse_modeline<'a>(&'a self) -> Option<Modeline> {
        let mut rest: &str = self.modeline_text.as_str();
        rest = rest.trim();

        // Strip "Modeline" prefix if present
        if let Some(cap) = self.re_modeline.captures(rest) {
            rest = cap.get(1).unwrap().as_str();
        }

        let name: &str = {
            let cap = match self.re_name.captures(rest) {
                Some(cap) => cap,
                None => return None,
            };
            rest = cap.get(2).unwrap().as_str();
            cap.get(1).unwrap().as_str()
        };

        let get_f64 = |rest: &mut &str| -> Option<f64> {
            let cap = match self.re_word.captures(*rest) {
                Some(cap) => cap,
                None => return None,
            };
            *rest = cap.get(2).unwrap().as_str();
            let text = cap.get(1).unwrap().as_str();
            match text.parse::<f64>() {
                Ok(x) => Some(x),
                Err(_) => return None,
            }
        };
        let get_i32 = |rest: &mut &str| -> Option<i32> {
            let cap = match self.re_word.captures(*rest) {
                Some(cap) => cap,
                None => return None,
            };
            *rest = cap.get(2).unwrap().as_str();
            let text = cap.get(1).unwrap().as_str();
            match text.parse::<i32>() {
                Ok(x) => Some(x),
                Err(_) => return None,
            }
        };

        let freq_mhz = get_f64(&mut rest)?;

        let hdisplay = get_i32(&mut rest)?;
        let hsync_start = get_i32(&mut rest)?;
        let hsync_end = get_i32(&mut rest)?;
        let htotal = get_i32(&mut rest)?;

        let vdisplay = get_i32(&mut rest)?;
        let vsync_start = get_i32(&mut rest)?;
        let vsync_end = get_i32(&mut rest)?;
        let vtotal = get_i32(&mut rest)?;

        let mut out = Modeline {
            name: name.to_owned(),
            mpx_per_s: freq_mhz,
            hdisplay,
            hsync_start,
            hsync_end,
            htotal,
            vdisplay,
            vsync_start,
            vsync_end,
            vtotal,
            interlace: false,
        };

        while let Some(cap) = self.re_word.captures(rest) {
            rest = cap.get(2).unwrap().as_str();
            let text = cap.get(1).unwrap().as_str().to_ascii_lowercase();
            if text == "interlace" {
                out.interlace = true;
            }
        }

        Some(out)
    }
}

fn click_to_copy(ui: &mut egui::Ui, text: String) {
    if ui.button(text.as_str()).clicked() {
        ui.output().copied_text = text;
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        use std::fmt::Write;

        egui::CentralPanel::default().show(ctx, |ui| {
            // i *really* dislike using a lambda instead of a block with destructor. a
            // lambda doesn't let rust know it will be called, so you can't easily pass
            // data out of the with_layout().
            //
            // but then c++ requires default-initialization too
            ui.with_layout(
                egui::Layout::top_down_justified(eframe::emath::Align::Min),
                |ui| {
                    ui.label("Modeline:");

                    // ui.add(egui::TextEdit::singleline
                    let response = ui.text_edit_singleline(&mut self.modeline_text);
                    if response.changed() {
                        self.modeline = self.parse_modeline();
                    }
                    if let Some(ref modeline) = &self.modeline {
                        click_to_copy(ui, format!("sampling rate {} Mpx/s", modeline.mpx_per_s));
                        let mut text = format!(
                            "px/line:\n{} px/line, sync=[{}, {}) of {}\n",
                            modeline.hdisplay,
                            modeline.hsync_start,
                            modeline.hsync_end,
                            modeline.htotal,
                        );

                        let us_per_px = 1. / modeline.mpx_per_s;
                        let line_per_s = modeline.mpx_per_s * 1_000_000. / modeline.htotal as f64;
                        let us_per_line = 1_000_000. / line_per_s;
                        write!(
                            text,
                            "\
{} k line/s
line = {} μs
video data = {} μs
front porch = {} μs
hblank = {} μs
back porch = {} μs",
                            line_per_s / 1000.,
                            us_per_line,
                            us_per_px * modeline.hdisplay as f64,
                            us_per_px * (modeline.hsync_start - modeline.hdisplay) as f64,
                            us_per_px * (modeline.hsync_end - modeline.hsync_start) as f64,
                            us_per_px * (modeline.htotal - modeline.hsync_end) as f64,
                        )
                        .unwrap();
                        click_to_copy(ui, text);

                        let mut text = format!(
                            "line/frame:\n{} line/frame, sync=[{}, {}) of {}\n",
                            modeline.vdisplay,
                            modeline.vsync_start,
                            modeline.vsync_end,
                            modeline.vtotal,
                        );

                        let mut fps = line_per_s / modeline.vtotal as f64;
                        if modeline.interlace {
                            fps *= 2.;
                        }
                        write!(
                            text,
                            "\
{} frame/s",
                            fps,
                        )
                        .unwrap();

                        click_to_copy(ui, text);
                    } else {
                        ui.label("Syntax error");
                    }
                },
            );
        });
    }
}
